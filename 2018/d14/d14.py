#!/usr/bin/python


def read_input(filename):
    with open(filename, 'r') as of:
        return list(map(int, of.readlines()))[0]


def read_input_2(filename):
    with open(filename, 'r') as of:
        return list(map(str.strip, of.readlines()))[0]


def print_status(elves, recipes):
    result = [' ' + str(r) + ' ' for r in recipes]
    result[elves[0]] = f'({result[elves[0]].strip()})'
    result[elves[1]] = f'[{result[elves[1]].strip()}]'
    to_print = ' '.join(['{:3s}'.format(r) for r in result])
    print(to_print)


def cook(elves, recipes):
    result = recipes[elves[0]] + recipes[elves[1]]
    if result < 10:
        recipes.append(result)
    else:
        recipes.extend([result // 10, result % 10])
    elves = [(e + recipes[e] + 1) % len(recipes) for e in elves]
    return elves, recipes


def calc(filename):
    n = read_input(filename)
    elves = [0, 1]
    recipes = [3, 7]
    while len(recipes) < n + 10:
        elves, recipes = cook(elves, recipes)
    return ''.join([str(i) for i in recipes[n:n+10]])


def calc2(filename):
    n = read_input_2(filename)
    elves = [0, 1]
    recipes = [3, 7]
    while n not in ''.join([str(r) for r in recipes[-len(n) - 1:]]):
        elves, recipes = cook(elves, recipes)
    last_recipes = ''.join([str(r) for r in recipes[-len(n) - 1:]])
    result = len(recipes) - len(n)
    return result - 1 + last_recipes.index(n)


if __name__ == '__main__':
    expected_results = ['0124515891', '5158916779', '9251071085', '5941429882']
    expected_results_2 = [5, 9, 18, 2018]
    for t in range(4):
        assert calc(f'test{t}') == expected_results[t]
        assert calc2(f'test2_{t}') == expected_results_2[t]
    print(calc('input'))
    print(calc2('input'))
