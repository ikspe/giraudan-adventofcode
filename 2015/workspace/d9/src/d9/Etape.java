package d9;

import java.util.List;

public class Etape {
	private String ville;
	private Integer distance;
	private List<String> next;

	public Etape(String ville, Integer distance, List<String> next) {
		this.ville = ville;
		this.distance = distance;
		this.next = next;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public Integer getDistance() {
		return distance;
	}

	public void setDistance(Integer distance) {
		this.distance = distance;
	}

	public List<String> getNext() {
		return next;
	}

	public void setNext(List<String> next) {
		this.next = next;
	}

	@Override
	public String toString() {
		return "Etape de " + ville + " (" + distance + " parcourus)";
	}
}
