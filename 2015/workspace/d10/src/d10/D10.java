package d10;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class D10 {

	private static final String IN = "1321131112";

	private static final Pattern P1 = Pattern.compile("(.)");
	private static final Pattern P2 = Pattern.compile("(.)\\1");
	private static final Pattern P3 = Pattern.compile("(.)\\1\\1");

	public static void main(String[] args) {
		System.out.println("7: " + IN);
		iterate(IN, 6);
	}

	private static void iterate(String in, int i) {
		if (i == 0)
			p("0: " + in);
		else {
			String subStr3 = in;
			Matcher m3 = P3.matcher(subStr3);
			List<String> res3 = new ArrayList<String>();
			List<Boolean> process3 = new ArrayList<Boolean>();
			int c3 = 0;
			boolean matches3 = false;
			while (m3.find()) {
				matches3 = true;
				c3 = m3.start();
				res3.add(subStr3.substring(0, c3));
				process3.add(true);
				res3.add("3" + m3.group(1));
				process3.add(false);
				subStr3 = subStr3.substring(c3 + 3);
				m3 = P3.matcher(subStr3);
			}
			if (matches3) {
				res3.add(in.substring(c3 + 3));
			} else {
				res3.add(in);
			}
			process3.add(true);

			while (res3.remove(""))
				;

			List<String> res2 = new ArrayList<String>();
			List<Boolean> process2 = new ArrayList<Boolean>();
			for (int j = 0; j < res3.size(); j++) {
				if (process3.get(j)) {
					String in2 = res3.get(j);
					String subStr = in2;
					Matcher m2 = P2.matcher(subStr);
					int c2 = 0;
					boolean matches2 = false;
					while (m2.find()) {
						matches2 = true;
						c2 = m2.start();
						res2.add(in2.substring(0, c2));
						process2.add(true);
						res2.add("2" + m2.group(1));
						process2.add(false);
						in2 = in2.substring(c2 + 2);
						m2 = P2.matcher(in2);
					}
					if (matches2) {
						res2.add(subStr.substring(c2 + 2));
					} else {
						res2.add(subStr);
					}
					process2.add(true);
				} else {
					res2.add(res3.get(j));
					process2.add(false);
				}
			}

			while (res2.remove(""))
				;

			List<String> res1 = new ArrayList<String>();
			for (int j = 0; j < res2.size(); j++) {
				if (process2.get(j)) {
					String subStr = res2.get(j);
					Matcher m1 = P1.matcher(subStr);
					res1.add(m1.replaceAll("1$1"));
				} else {
					res1.add(res2.get(j));
				}
			}

			while (res1.remove(""))
				;

			String result = "";
			for (String r : res1) {
				result += r;
			}
			p(i + ": " + result);
			iterate(result, i - 1);
		}
	}

	private static void p(String s) {
		System.out.println(s);
	}
}
