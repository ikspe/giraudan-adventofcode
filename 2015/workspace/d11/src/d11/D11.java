package d11;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class D11 {

	//private static final String INPUT = "cqjxjnds";
	private static final String INPUT = "cqjxxyzz";

	private static final List<Integer> FORBIDDEN = new ArrayList<Integer>() {
		private static final long serialVersionUID = -1636795691138596429L;

		{
			add(105); // i
			add(108); // l
			add(111); // o
		}
	};

	public static void main(String[] args) {
		byte[] pwd = INPUT.getBytes(StandardCharsets.US_ASCII);
		pwd = increment(pwd, 7);
		while (!checkPwd(pwd)) {
			pwd = increment(pwd, 7);
		}
		disp(pwd);
	}

	/**
	 * 97 = a; 122 = z
	 */
	private static byte[] increment(byte[] pwd, int idx) {
		pwd[idx] = (byte) (pwd[idx] + 1);
		if (FORBIDDEN.contains(pwd[idx])) {
			pwd[idx] = (byte) (pwd[idx] + 1);
		}
		if (pwd[idx] > 122) {
			pwd[idx] = 97;
			if (idx > 0) {
				pwd = increment(pwd, idx - 1);
			}
		}
		return pwd;
	}

	private static boolean checkPwd(byte[] pwd) {

		// i, o and l
		for (byte b : pwd) {
			if (FORBIDDEN.contains(b)) {
				return false;
			}
		}

		// 2 pairs
		int nbPairs = 0;
		List<Integer> usedPairLetters = new ArrayList<Integer>();
		for (int i = 0; i < pwd.length - 1; i++) {
			if (pwd[i] == pwd[i + 1]) {
				if ((i > 0 && pwd[i - 1] == pwd[i]) || usedPairLetters.contains(pwd[i])) {
					// Do nothing
				} else {
					usedPairLetters.add((int) pwd[i]);
					nbPairs++;
				}
			}
		}
		if (nbPairs < 2)
			return false;

		// 3 follow
		for (int i = 0; i < pwd.length - 2; i++) {
			if (pwd[i] == pwd[i + 1] - 1 && pwd[i + 1] == pwd[i + 2] - 1)
				return true;
		}

		return false;
	}

	private static void disp(byte[] pwd) {
		System.out.println(new String(pwd, StandardCharsets.US_ASCII));
	}

}
