package d19;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class D19 {

	private static Map<String, String> transformations = new HashMap<String, String>();

	private static List<String> solutions = new ArrayList<String>();

	private static final Pattern P = Pattern.compile("([a-zA-Z]*) => ([a-zA-Z]*)");

	public static void main(String[] args) {
		BufferedReader b = null;
		try {
			// input
			File f = new File("input");
			b = new BufferedReader(new FileReader(f));
			String line = null;
			while ((line = b.readLine()) != null) {
				Matcher m = P.matcher(line);
				if (m.matches()) {
					String t_from = m.group(1);
					String t_to = m.group(2);
					transformations.put(t_to, t_from);
				} else {
					System.err.println("ERROR: '" + line + "' does not match the expected pattern");
				}
			}
			b.close();

			// molecule
			f = new File("molecule");
			b = new BufferedReader(new FileReader(f));
			String molecule = b.readLine();
			b.close();

			for (Entry<String, String> t : transformations.entrySet()) {
				String t_from = t.getValue();
				String t_to = t.getKey();
				System.out.println("Processing transformation " + t_from + " -> " + t_to + "...");
				int instanceIdx = molecule.indexOf(t_from, 0);
				while (instanceIdx >= 0) {
					String result = molecule.substring(0, instanceIdx) + t_to
							+ molecule.substring(instanceIdx + t_from.length());
					if (!solutions.contains(result))
						solutions.add(result);
					instanceIdx = molecule.indexOf(t_from, ++instanceIdx);
				}
			}
			System.out.println(solutions.size());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
