import re
import collections
room_pattern = re.compile('([a-z-]+)([0-9]+)\[([a-z]{5})\]')
f = open('input', 'r')
rooms = f.readlines()
for room in rooms:
    match = room_pattern.match(room.strip())
    enc_room_name = match.group(1)
    letters = enc_room_name.replace('-', '')
    room_id = int(match.group(2))
    checksum = match.group(3)
    letters_freq = collections.Counter(letters)
    five_most = letters_freq.most_common(5)
    min_cnt = five_most[len(five_most) - 1][1]
    count_this = True
    for l in checksum:
        count_this = count_this and letters_freq[l] >= min_cnt
    for l in five_most:
        if letters_freq[l[0]] > min_cnt:
            count_this = count_this and l[0] in checksum
    if count_this:
        shift = room_id % 26
        lb = list(bytearray(enc_room_name))
        for i in range(len(lb)):
            if lb[i] > 50:
                lb[i] = lb[i] + shift
                if lb[i] > 122:
                    lb[i] = lb[i] - 26
        print('%d - %s' % (room_id, ''.join(chr(x) for x in lb)))
