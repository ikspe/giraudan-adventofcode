import rlcompleter
import readline
readline.parse_and_bind("tab: complete")

'''
return value: (direction, [ indexes_to_move ])
'''
def valid_moves(building, elev_pos):
    moves = []
    if elev_pos < 0 or elev_pos > 3:
        return moves
    floor = building[elev_pos]
    m_chips = [i for i,x in enumerate(floor[1::2]) if x == 1]
    gens = [i for i,x in enumerate(floor[::2]) if x == 1]
    if elev_pos > 0:
        #print('MOVE BELOW FROM %d TO %d' % (elev_pos + 1, elev_pos))
        below_floor = building[elev_pos - 1]
        below_m_chips = [i for i,x in enumerate(below_floor[1::2]) if x == 1]
        below_gens = [i for i,x in enumerate(below_floor[::2]) if x == 1]
        
        for m in m_chips:
            #print('considering chip %d' % m)
            if m in below_gens or len(below_gens) == 0:
                #print('  OK to move chip %d alone' % m)
                moves.append((-1, [2*m+1]))
            if m in gens:
                move_gen_ok = True
                for mm in below_m_chips:
                    if mm != m and mm not in below_gens:
                        #print('    KO: cannot move chip and gen %d because of chip %d' % (m, mm))
                        move_gen_ok = False
                        break
                if move_gen_ok:
                    #print('  OK to move both chip and gen %d' % m)
                    moves.append((-1, [2*m, 2*m+1]))
            for mm in m_chips:
                if m > mm:
                    if (m in below_gens and mm in below_gens) or len(below_gens) == 0:
                        #print('  OK to move chips %d and %d' % (m, mm))
                        moves.append((-1, [2*m+1, 2*mm+1]))
        for g in gens:
            #print('considering gen %d' % g)
            move_gen_ok = (g not in m_chips or len(gens) < 2)
            #print('  a priori ok: %s' % move_gen_ok)
            if move_gen_ok:
                for m in below_m_chips:
                    #print('    checking with chip %d' % m)
                    if m != g and m not in below_gens:
                        #print('      conflict between chip %d and gen %d' % (m, g))
                        move_gen_ok = False
                        break
            if move_gen_ok:
                #print('  OK to move gen %d alone' % g)
                moves.append((-1, [2*g]))
                for gg in gens:
                    if g > gg:
                        move_gg_ok = gg not in m_chips
                        if move_gg_ok:
                            for mm in below_m_chips:
                                if mm != gg and mm not in below_gens:
                                    move_gg_ok = False
                                    break
                        if move_gg_ok:
                            moves.append((-1, [2*g, 2*gg]))

    if elev_pos < 3:
        #print('MOVE ABOVE FROM %d TO %d' % (elev_pos + 1, elev_pos + 2))
        above_floor = building[elev_pos + 1]
        above_m_chips = [i for i,x in enumerate(above_floor[1::2]) if x == 1]
        above_gens = [i for i,x in enumerate(above_floor[::2]) if x == 1]

        for m in m_chips:
            #print('considering chip %d' % m)
            if m in above_gens or len(above_gens) == 0:
                #print('  OK to move chip %d alone' % m)
                moves.append((1, [2*m+1]))
            if m in gens:
                move_gen_ok = True
                for mm in above_m_chips:
                    if mm != m and mm not in above_gens:
                        #print('    KO: cannot move chip and gen %d because of chip %d' % (m, mm))
                        move_gen_ok = False
                        break
                if move_gen_ok:
                    #print('  OK to move both chip and gen %d' % m)
                    moves.append((1, [2*m, 2*m+1]))
            for mm in m_chips:
                if m > mm:
                    if (m in above_gens and mm in above_gens) or len(above_gens) == 0:
                        #print(' OK to move chips %d and %d' % (m, mm))
                        moves.append((1, [2*m+1, 2*mm+1]))
        for g in gens:
            #print('considering gen %d' % g)
            move_gen_ok = (g not in m_chips or len(gens) < 2)
            #print('  a priori ok: %s' % move_gen_ok)
            if move_gen_ok:
                for m in above_m_chips:
                    #print('    checking with chip %d' % m)
                    if m != g and m not in above_gens:
                        #print('      conflict between chip %d and gen %d' % (m, g))
                        move_gen_ok = False
                        break
            if move_gen_ok:
                #print('  OK to move gen %d alone' % g)
                moves.append((1, [2*g]))
                for gg in gens:
                    if g > gg:
                        move_gg_ok = gg not in m_chips
                        if move_gg_ok:
                            for mm in above_m_chips:
                                if mm != gg and mm not in above_gens:
                                    move_gg_ok = False
                                    break
                        if move_gg_ok:
                            moves.append((1, [2*g, 2*gg]))

    #print('valid moves: %s' % moves)
    return moves

def make_move(building, elev_pos, move):
    floor = building[elev_pos]
    other_floor = building[elev_pos + move[0]]
    for item in move[1]:
        floor[item] = 0
        other_floor[item] = 1
    building[elev_pos] = floor
    building[elev_pos + move[0]] = other_floor
    return (building, elev_pos + move[0])

def print_building(building, elev_pos):
    #print('')
    #print('  Pro   Cob   Cur   Rut   Plu')
    #print('F|G M   G M   G M   G M   G M')
    for y in range(len(building)):
        line = str(4 - y) + '|'
        for x in range(len(building[3 - y])):
            if x % 2 == 0:
                line += '%-2d' % building[3 - y][x]
            else:
                line += '%-4d' % building[3 - y][x]
        if y == 3 - elev_pos:
            line += ' *'
        #print(line)
    #print('Hash: %d' % hash_building(building))
    #print('')

def deep_copy(array):
    return [ row[:] for row in array ]

def hash_building(building):
    bin_str ='0b'
    for y in range(4):
        bin_str += ''.join(list(map(str, building[y])))
    return int(bin_str, 2)

if __name__ == '__main__':
    init_building = [ [ 0 for x in range(10) ] for y in range(4) ]
    '''
    x   0   1   2   3   4   5   6   7   8   9

y   F   Pro     Cob     Cur     Rut     Plu
        G   M   G   M   G   M   G   M   G   M
0   1   1   1
1   2           1       1       1       1
2   3               1       1       1       1
3   4
    '''
    init_building[0][0] = 1
    init_building[0][1] = 1
    init_building[1][2] = 1
    init_building[2][3] = 1
    init_building[1][4] = 1
    init_building[2][5] = 1
    init_building[1][6] = 1
    init_building[2][7] = 1
    init_building[1][8] = 1
    init_building[2][9] = 1

    target = [ [ 0 for x in range(10) ] for y in range(4) ]
    target[3][0] = 1
    target[3][1] = 1
    target[3][2] = 1
    target[3][3] = 1
    target[3][4] = 1
    target[3][5] = 1
    target[3][6] = 1
    target[3][7] = 1
    target[3][8] = 1
    target[3][9] = 1
    target[3][9] = 1

    elev_pos = 0

    init_hash = hash_building(init_building)
    building = deep_copy(init_building)
    queue = [ (building, elev_pos) ]
    visited = [ (init_hash, elev_pos) ]
    tree = { }
    count = 0
    while queue:
        count += 1
        (building, elev_pos) = queue.pop(0)
        h = hash_building(building)
        if building == target:
            fallback = h
            cpt = 0
            while fallback != init_hash:
                fallback = tree[fallback]
                cpt += 1
            print(cpt)
            break
        print_building(building, elev_pos)
        for m in valid_moves(building, elev_pos):
            (new_building, new_elev_pos) = make_move(deep_copy(building), elev_pos, m)
            new_hash = hash_building(new_building)
            if (new_hash, new_elev_pos) not in visited:
                visited.append((new_hash, new_elev_pos))
                queue.append((new_building, new_elev_pos))
                tree.update({new_hash: h})

