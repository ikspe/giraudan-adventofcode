#!/usr/bin/python

def isprime(n):
    i = 2
    while i * i <= n:
        if n % i == 0:
            return 0
        i += 1
    return 1

def calc2(offset = 108100, diff = 1000):
    return diff + 1 - sum([isprime(x) for x in range(offset, offset + 17 * (diff + 1), 17)])

def calc(filename):
    with open(filename, 'r') as f:
        instrs = [x.split(' ') for x in f.read().strip().split('\n')]
        pointer = 0
        regs = {chr(x):0 for x in range(97,105)}
        nb_mul = 0
        while pointer in range(len(instrs)):
            i = instrs[pointer]
            x = int(i[1]) if i[1].replace('-','').isdigit() else regs[i[1]]
            y = int(i[2]) if i[2].replace('-','').isdigit() else regs[i[2]]
            if i[0] == 'set':
                regs[i[1]] = y
            elif i[0] == 'sub':
                regs[i[1]] -= y
            elif i[0] == 'mul':
                nb_mul += 1
                regs[i[1]] *= y
            elif i[0] == 'jnz' and x != 0:
                pointer += y - 1
            pointer += 1
        return nb_mul

if __name__ == '__main__':
    print('Answer 1: %d' % calc('input'))
    print('Answer 2: %d' % calc2())
