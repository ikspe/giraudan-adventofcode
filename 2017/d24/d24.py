#!/usr/bin/python

def weight(path, weights):
    return sum([weights[x] for x in path])

def iterate(to_visit, comps):
    visited = []
    while len(to_visit) > 0:
        p = to_visit.pop()
        cur_w = p['cur_w']
        i = p['comp']
        candidates = comps[i]['w'].copy()
        candidates.remove(cur_w)
        other_values = [x for x in candidates]
        if len(other_values) == 0:
            visited.append(p)
            continue
        other_value = other_values[0]
        nexts = [x for x in comps if other_value in comps[x]['w'] and x != i and x not in p['path']]
        for n in nexts:
            new_path = p['path'] + [n]
            to_visit.append({'comp': n, 'path': new_path, 'cur_w': other_value})
        visited.append(p)
    return visited

def calc(filename):
    with open(filename, 'r') as f:
        comps = {k:{'w':[int(x) for x in v.split('/')]} for k,v in enumerate(f.read().strip().split('\n'))}
        weights = {k: sum(comps[k]['w']) for k in comps}
        start_path = [{'cur_w': 0, 'comp': x, 'path': [x]} for x in comps if 0 in comps[x]['w']]
        bridges = iterate(start_path, comps)
        a1 = max([weight(x['path'], weights) for x in bridges])
        L = max([len(x['path']) for x in bridges])
        a2 = max([weight(x['path'], weights) for x in bridges if len(x['path']) == L])
        return (a1, a2)

if __name__ == '__main__':
    assert calc('test') == (31, 19)
    print('Answer 1: %d\nAnswer 2: %d' % calc('input'))
